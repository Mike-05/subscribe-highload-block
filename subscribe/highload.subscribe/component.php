<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CUser $USER */

global $APPLICATION;
use Bitrix\Main\Loader; 

Loader::includeModule("highloadblock"); 
use Bitrix\Highloadblock as HL; 

// Массив $arResult
$arResult = array();

$arSortedProjectList = array();

$query = ltrim($_REQUEST["q"] ?? '');

$arrFilter = array();

if (!empty($query) && is_numeric($query)) {
    $arrFilter = array("UF_ID" => $query);
    $errorSearchResult = true;
} elseif(!empty($query)) {
    $arrFilter = array("UF_PROJECT_NAME" => $query); 
    $errorSearchResult = true;
}


class getHighLoadBlockElements
{
    /**
     * Метод Возвращает элементы HighLoadBlocks
     * @var array $HLIBLOCK_ID - идентификатор товара
     * @var array $hlBlocksElements - массив с id подарков для товара
     * @var array $hlPageCount - Количества элементов на страницы 
     * Global CMain $APPLICATION
     */

    public static function getProjectList($HLIBLOCK_ID, $arrFilter, $query, $errorSearchResult) {
        $hlBlocksElements = [];

        if (!$HLIBLOCK_ID) {
            return $hlBlocksElements;
        }
       
        $projectHlBlock = HL\HighloadBlockTable::getById($HLIBLOCK_ID)->fetch(); // Указываем ID нашего highloadblock блока к которому будет делать запросы.
        $projectHlEntity = HL\HighloadBlockTable::compileEntity($projectHlBlock); 
        $projectHlEntityData = $projectHlEntity->getDataClass();

        $projectData = $projectHlEntityData::getList(array(
            "select" => array("*"),
            "order" => array("ID" => "ASC"),
            'cache' => ['ttl' => 86400],
            "filter" => $arrFilter,
            
        ));

        while ($arData = $projectData->Fetch()) {
            $errorSearchResult = false;

            // Если deadline не пустой, то записываем
            if (!empty($arData['UF_FINISH_DATE'])) {
                $arDateTime = ParseDateTime($arData['UF_FINISH_DATE'], FORMAT_DATETIME);
                $arData['UF_FINISH_DATE'] = $arDateTime["DD"]." ".ToLower(GetMessage("MONTH_".intval($arDateTime["MM"])."_S"))." ".$arDateTime["YYYY"]. "," . $arDateTime['HH'];
            }
    
            // Если элемент не пустой, то записываем в массив
            if (!empty($arData)) {
                $hlBlocksElements['PROJECT_LIST'][$arData['ID']] = $arData;
            }
         }

         if(!empty($query) && $errorSearchResult) {
            $hlBlocksElements['SEARCH_ERROR'] = "К сожалению, на ваш поисковый запрос  ". $query . " ничего не найдено.";
            $hlBlocksElements['SEARCH_QUERY'] = $query;
         }

        return $hlBlocksElements;
    }

    public static function getSubscribe($HLIBLOCK_ID, $userId) {

        $hlSubscribeElements = [];

        if (!$HLIBLOCK_ID) {
            return $hlSubscribeElements;
        }

        $subscribeHlBlock = HL\HighloadBlockTable::getById($HLIBLOCK_ID)->fetch(); // Указываем ID нашего highloadblock блока к которому будет делать запросы.
        $subscribeHlEntity = HL\HighloadBlockTable::compileEntity($subscribeHlBlock); 
        $subscribeHlEntityData = $subscribeHlEntity->getDataClass();

        $subscribeData = $subscribeHlEntityData::getList(array(
            "select" => array("*"),
            "order" => array("ID" => "ASC"),
            'cache' => ['ttl' => 86400],
            'filter' => array ("UF_USER_ID" => $userId),
        ));

        while ($arData = $subscribeData->Fetch()) {
            // Если элемент не пустой, то записываем в массив
            if (!empty($arData)) {
                $hlSubscribeElements['SUBSCRIBE_LIST'][$arData['ID']] = $arData;
                

                if ($arData['UF_EMAIL_SUBSCRIBE']) {
                    $hlSubscribeElements['SUBSCRIBED_PROJECT_LIST'][$arData['UF_ID_PROJECTS']] = $arData['UF_ID_PROJECTS'];
                }
                           
            }
         }

        return $hlSubscribeElements;

    }
}

$arHighLoadBlockElements = new getHighLoadBlockElements();

// Получаем массив Список проектов 
if (!empty($arParams['IBLOCK_PROJECTLIST_ID']) && is_numeric($arParams["IBLOCK_PROJECTLIST_ID"])) {
    $arResult['IBLOCK_PROJECTLIST_ID'] = $arParams['IBLOCK_PROJECTLIST_ID'];
    $arResult = $arHighLoadBlockElements->getProjectList($arParams['IBLOCK_PROJECTLIST_ID'], $arrFilter, $query, $errorSearchResult);
}

// Получаем массив Список подписок  
if (!empty($arParams['IBLOCK_SUBSCRIBE_ID']) && is_numeric($arParams["IBLOCK_SUBSCRIBE_ID"])) {
    $arResult['IBLOCK_SUBSCRIBE_ID'] = $arParams['IBLOCK_SUBSCRIBE_ID'];
    $arResult['SUBSCRIBE'] = $arHighLoadBlockElements->getSubscribe($arParams['IBLOCK_SUBSCRIBE_ID'], $USER->GetID());
}

if (!empty($arParams['ELEMENTS_COUNT'])) {
    $arResult['~ELEMENTS_COUNT'] = $arParams['~ELEMENTS_COUNT'];
    $arResult['ELEMENTS_COUNT'] = $arParams['ELEMENTS_COUNT'];
}


if (!empty($arResult['SUBSCRIBE']['SUBSCRIBED_PROJECT_LIST'])) {
    foreach($arResult['SUBSCRIBE']['SUBSCRIBED_PROJECT_LIST'] as $arProjectListId) {
        if($arResult['PROJECT_LIST'][$arProjectListId]['ID'] == $arProjectListId) {
            $arResult['PROJECT_LIST'][$arProjectListId]['CHECKED'] = "Y";   
        }

        if ($_GET['subscribe'] === "Y") {
            if ($arResult['PROJECT_LIST'][$arProjectListId]['CHECKED'] === "Y") {
                $arSortedProjectList[$arProjectListId] = $arResult['PROJECT_LIST'][$arProjectListId];
            }            
        }
    }
}

if (isset($arSortedProjectList) && !empty($arSortedProjectList)) {
    $arResult['PROJECT_LIST'] = $arSortedProjectList;
}

$this->IncludeComponentTemplate();

