$(document).ready(function() { 
    $(document).on('click', '#checkbox', function() { 
        sendSubscribtionEvent($(this))
    });
    
    function sendSubscribtionEvent(param) {
        let url = '/bitrix/components/subscribe/highload.subscribe/templates/ajax.php';
        let projectId = $(param).attr('data-project');
        let userId = $(param).attr('data-user');
        let val = $(param).val();

        if (url !== undefined) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'html',
                data: { 'user-id': userId, 'project-id': projectId, 'val':val},
                success: function(result) { 

                }
            })
        }
    }
});