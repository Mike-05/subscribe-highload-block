<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); 
use Bitrix\Main\Localization\Loc;?>

<?php if(!empty($arResult)):?>
	<div class="project_block">
		<div class="project_block_title"><?=Loc::getMessage("SUBSCRIBE_TITLE");?></div>
		<div class="project_block_top">
			<div class="project_block_top-title"><?=Loc::getMessage("SUBSCRIBE_SUBTITLE");?></div>
			<div class="project_block_top_wrap">
				<form class="project_block_top_form" method="get">
					<input type="text" name="q" placeholder="Искать здесь...">
					<button type="submit">
					<svg class="search__img" width="16" height="16" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M14 14L19 19" stroke="#686868" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
						<path fill-rule="evenodd" clip-rule="evenodd" d="M16 8.5C16 12.6415 12.6422 16 8.49949 16C4.35775 16 1 12.6415 1 8.5C1 4.35752 4.35775 1 8.49949 1C12.6422 1 16 4.35752 16 8.5Z" stroke="#686868" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
					</svg>
					</button>
				</form>
				<a href="<?php if($_GET['subscribe'] == "Y"):?> ?subscribe=N <?php else:?> ?subscribe=Y <?php endif;?>" class="project_block_top_sort_btn <?php if($_GET['subscribe'] == "Y"):?> active <?php endif;?>">
					<?php if($_GET['subscribe'] == "Y"):?>
						<?=Loc::getMessage("SUBSCRIBE_BTN_SORTED");?>
					<?php else:?>
						<?=Loc::getMessage("SUBSCRIBE_SORT_BTN");?>
					<?php endif;?>
				</a>
			</div>
		</div>	
		<?php if(!empty($arResult['PROJECT_LIST'])):?>
			<div class="project_block">
				<div class="project_block_wrap">
					<div class="project_block_wrap_head">
						<div class="project_block_code">
							<div class="project_block_code_title"><?=Loc::getMessage("PROJECT_LIST_CODE");?></div>	
						</div>
						<div class="project_block_name">
							<div class="project_block_name_title"><?=Loc::getMessage("PROJECT_LIST_NAME");?></div>
						</div>
						<div class="project_block_email">
							<div class="project_block_email_title"><?=Loc::getMessage("PROJECT_LIST_EMAIL");?></div>
						</div>
					</div>
					
					<?php foreach($arResult['PROJECT_LIST'] as $arProjectList):?>
						<div class="project_block_wrap_body">
							<div class="project_block_code">
								<div class="project_block_code_value"><?=$arProjectList['UF_ID'];?></div>	
							</div>
							<div class="project_block_name">
								<div class="project_block_name_value"><?=$arProjectList['UF_PROJECT_NAME']?></div>
							</div>
							<div class="project_block_email">
								<div class="project_block_email_checkbox">
									<input type="checkbox" id="checkbox" <?php if ($arProjectList['CHECKED'] == "Y"):?> checked value="<?=$arProjectList['CHECKED'];?>" <? endif;?> data-project="<?=$arProjectList['ID'];?>" data-user="<?=$USER->GetID();?>">
								</div>
							</div>
						</div>
					<?php endforeach;?>
					
				</div>
			</div>
		<?php elseif(!empty($arResult['SEARCH_QUERY']) && !empty($arResult['SEARCH_ERROR'])):?>
			<p class="search-error"><?=$arResult['SEARCH_ERROR'];?></p>
		<?php endif;?>
	</div>
<?php endif;?>


