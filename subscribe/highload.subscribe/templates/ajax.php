<?php 
/** @global CUser $USER */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

//D7
use Bitrix\Main\Loader,
Bitrix\Highloadblock as HL;
Loader::includeModule("highloadblock"); 

// Id Subscribe блока
$ID = 12;
$productId = $_POST['project-id'];

$hasNoElements = true;

if (!empty($_POST)) {
    $hlblock = HL\HighloadBlockTable::getById($ID)->fetch(); //где ID - id highloadblock блока из которого будем получать данные
    $entity = HL\HighloadBlockTable::compileEntity($hlblock); 
    $entity_data_class = $entity->getDataClass(); 

    $data = $entity_data_class::getList(array(
        "select" => array("*"),
        "order" => array("ID" => "ASC"),
        "filter" => array("UF_USER_ID" => $USER->GetID(), "UF_ID_PROJECTS" => $productId) //Фильтрация выборки
    ));

    
    while ($arData = $data->Fetch()) {   
        if (!empty($arData) && isset($arData)) {
            $hasNoElements = false;
            
            // if ($arData['UF_EMAIL_SUBSCRIBE']) {
            //     $updateSubscribe = array("UF_EMAIL_SUBSCRIBE" => 0);
            // } else {
            //     $updateSubscribe = array("UF_EMAIL_SUBSCRIBE" => 1);
            // }

            $entity_data_class::Delete($arData['ID']);

            //$result = $entity_data_class::update($arData['ID'], $updateSubscribe); // где $ELEMENT_ID -  id обновляемой записи 
        }
    }

    if ($hasNoElements) {
        //Массив добавляемых параметров
        $newElementData = array(
            "UF_USER_ID" => $USER->GetID(), 
            "UF_ID_PROJECTS" => $productId,
            "UF_EMAIL_SUBSCRIBE" => 1
        );
        
        $result = $entity_data_class::add($newElementData);
        
        if ($result->isSuccess()) {
            echo 'успешно добавлен';
        } else {
            echo 'Ошибка: ' . implode(', ', $otvet->getErrors()) . "";
        }
    }
}

